//nice scroll//

(function() {
  $(document).ready(function() {
    $(".navbar .list .list__item a").click(function(e) {
      e.preventDefault();
      var name = "#" + $(this).attr("data-scroll");
      $("html, body").animate(
        {
          scrollTop: $(name).offset().top
        },
        2500
      );
    });

    //menu
    $(".menu-section").click(function(e) {

      e.preventDefault();
      $(".menu__btn").click();
      $("#click").attr("checked", false);

      var name = "#" + $(this).attr("data-scroll");

      $("html, body").animate(
        {
          scrollTop: $(name).offset().top + 1
        },
        2500
      );
    });


    var width = $(window).width();

    if(width <=600) {
      changeFooterList();
    }
    $(window).on('resize', function() {
      if ($(this).width() !== width) {
        width = $(this).width();
        if(width <=600) {
          changeFooterList();
        } else {
          changeTodisktopDesign();
        }
      }
    });

  });

  function changeFooterList() {
    let ele = $('.footer').children('.grid').first();
    $(ele).removeClass('grid').addClass('footer__list');
    let children = $(ele).children();
    children.each(function(id,item) {
      $(item).addClass('footer__list-item');
      $(item).children('.menu-list').hide();
      let arraw = $("<img src='/img/line-angle-down.svg' alt=''>");
      $(item).append(arraw);
    });
  }
  
  function changeTodisktopDesign() {
    let ele = $('.footer').children('.footer__list').first();
    $(ele).removeClass('footer__list').addClass('grid');
    let children = $(ele).children();
    children.each(function(id,item) {
      $(item).removeClass('footer__list-item');
      $(item).children('.menu-list').show();
      $(item).children('img').remove();
    });
  }
})();

class Subscription {
  
  id;
  title;
  subTitle;
  price;
  discountAmount;
  features;

  constructor(subscription) {
    
    this.id = subscription.id || 0;
    this.title = subscription.title || "";
    this.subTitle = subscription.subTitle || "";
    this.price = subscription.price || 0;
    this.features = subscription.features || [];
    this.discountAmount = subscription.discountAmount || "-";

  }

}

//MVC////

var viewControl = (function() {


  const DOMStrings =  {
    subscriotionsGrid:".subscription .subscription__content .grid",
    gridLab: ".subscription__content .card__footer .btn"
  };


  function addFeatures(features) {

    let featureList = ``;
    features.forEach(feature=>{
      featureList+=addFeature(feature);
    })
    return featureList;

  };

  function addFeature(feature) {
    
    let _feature = `
    <li class="v-list__item">
      <div class="v-list__icon">
          <img src="/img/yes-icon.png" alt="">
      </div>
      <span>${feature}</span>
   </li>`;

   return _feature;

  }

  function getList(button) {
    return $(button).parent('.card__footer').siblings('.card__body').find('.v-list').get(0);
  }

  function clearList(list) {
    $(list).empty();
  }
  
  function displayFeatures(features,list) {
    let _features = '';
    _features+=addFeatures(features);
    clearList(list);
    list.insertAdjacentHTML('beforeEnd',_features);

  }


  function getPackageId(button) {
    return $(button).data('id');
  }

   function displayAllPackages(packages) {

    let HtmlElement = "";

    packages.forEach(package=>{
      HtmlElement = `
      <div class="grid__cell--1 card" data-id = ${package.id}>
      <div class="card__header">
          <h1 class="headding-2">
              ${package.title}
          </h1>
          <p class="heading-4">${package.subTitle}</p>
      </div>
      <div class="card__body padding-bottom-u-s">
          <div class="price">
              <h1 class="headding-2">
              ${package.price}
              </h1>
              <p class="heading-4">سنويا</p>
          </div>
          <h1 class="heading-3 margin-bottom-u-s">
          ${package.discountAmount}
          </h1>
          <button class="btn btn__primary btn__lg btn__block margin-bottom-u-s">
              اشترك الآن
          </button>
          <ul class="v-list ml-auto">
      `

      let featuresList;
      let status;
      if($(window).width() > 600)
       {
         featuresList = addFeatures(package.features.slice(0));
         status = 'showLess';
       }
      else {
        featuresList = addFeatures(package.features.slice(0,2));
        status = 'showMore';
      }


      HtmlElement+=featuresList;

      HtmlElement+=`</ul></div>`;

      HtmlElement+=`
      <div class="card__footer">
        <button class="btn btn__secondary btn__lg btn__block" data-id=${package.id} data-status="${status}">
            <span class="margin-bottom-u-vs">عرض كل ميزات الاشتراك</span>
            <img src="/img/line-angle-down.svg" alt="">
        </button>
      </div>`;

      document.querySelector(DOMStrings.subscriotionsGrid)
      .insertAdjacentHTML('beforeEnd',HtmlElement);

    })
  }

  function getButtonStatus(button) {
    return $(button).data('status');
  }
  
  function changeButtonTitle(button) {

    let text = '';
    let status = getButtonStatus(button);

    if(status == 'showMore') {
      text = 'عرض عناصر أقل';
      $(button).data('status','showLess');
    } else if(status == 'showLess') {
      text = 'عرض كل ميزات الاشتراك';
      $(button).data('status','showMore');
    }
    $(button).children('span').text(text);
    $(button).children('img').toggleClass('btn__rotate');

  }

  return {
    DOMStrings,
    displayAllPackages,
    displayFeatures,
    getPackageId,
    getList,
    changeButtonTitle,
    getButtonStatus
  };

})();

var modelControl = (function() {
  
  const subscriptions = [
    {
      id:1,
      title:"اشتراك سنوي",
      subTitle:"اشتراك رقمي فقط",
      price:"$73",
      discountAmount:"أفضل قيمة | وفر 47 دولار",
      features:[
        "وصول رقمي المحتوى عبر التطبيق والموقع",
        "30 اصدار من المجلة",
        "300 فيديو قيم يثري معارلك",
        "5 آلاف مقالة تزدات يوميا",
        "مكتبة المحتوى الصوتي",
        "نشرة بريدية خاصة بالأعضاء"
      ]
    },
    {
      id:2,
      title:"اشتراك 6 أشهر",
      subTitle:"اشتراك رقمي فقط",
      price:" كل سنة أشهر $73",
      discountAmount:"وفر 28 دولار",
      features:[
        "وصول رقمي المحتوى عبر التطبيق والموقع",
        "30 اصدار من المجلة",
        "300 فيديو قيم يثري معارلك",
        "5 آلاف مقالة تزدات يوميا",
        "مكتبة المحتوى الصوتي",
        "نشرة بريدية خاصة بالأعضاء"
      ]
    },
    {
      id:3,
      title:"اشتراك شهري",
      subTitle:"اشتراك رقمي فقط",
      price:"10$ شهريا",
      features:[
        "وصول رقمي المحتوى عبر التطبيق والموقع",
        "30 اصدار من المجلة",
        "300 فيديو قيم يثري معارلك",
        "5 آلاف مقالة تزدات يوميا",
        "مكتبة المحتوى الصوتي",
        "نشرة بريدية خاصة بالأعضاء"
      ]
    }
  ].map(item=>{
    return new Subscription(item);
  });

  function getAllSubscriptions() {
    return subscriptions;
  }

  function getSubscriptionById(id) {
    
    if(!id) {
      return {};
    } 
    return subscriptions.find(item=>item.id == id);

  }


  function getFeatures(packageId) {
    return getSubscriptionById(packageId)['features'];
  }
  
  return {
    getAllSubscriptions,
    getSubscriptionById,
    getFeatures
  };

})();

var Controller = (function(UICtrl, MCtrl) {

  var domSettings = UICtrl.DOMStrings;
  
  var eventListener = function () {
    $(domSettings.gridLab).on('click',function() {
      displayFeatures(this);
    })
  }

  function displayFeatures(button) {
    packageId = UICtrl.getPackageId(button);
    list = UICtrl.getList(button);
    let features = MCtrl.getFeatures(packageId);
    const status = UICtrl.getButtonStatus(button);
    UICtrl.changeButtonTitle(button);
    if(status == 'showLess') {
      features = features.slice(0,2);
    }
    UICtrl.displayFeatures(features,list);
  }

  function initContent() {
    const packagesInfo = MCtrl.getAllSubscriptions();
    if(packagesInfo) {
      UICtrl.displayAllPackages(packagesInfo);
    }
  }
  return {
    init:function() {
      initContent();
      eventListener();
    }
  };
})(viewControl, modelControl);

Controller.init();
